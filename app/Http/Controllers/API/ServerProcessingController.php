<?php

namespace App\Http\Controllers\API;

use DataTables;
use App\Models\Car;
use App\Models\Trademark;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServerProcessingController extends Controller
{
    public function getAllTrademarks(Request $request) {
        return DataTables::of(Trademark::all())->toJson();
    }

    public function getAllCars(Request $request) {
        return DataTables::of(Car::with('trademark'))->toJson();
    }
}
