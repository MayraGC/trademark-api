<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Http\Controllers\Controller;
use App\Models\Trademark;
use Illuminate\Http\Request;

class TrademarkController extends Controller
{
    private $attributes = ['title'];

    private $messages = [
        'title.required' => 'El titulo es requerida.',
        'title.unique' => 'El titulo debe ser único.',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Trademark::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'         => 'bail | required | unique:trademarks,title'
        ], $this->messages);

        $response = ['success' => 'Creación completada exitosamente.'];
        $code = 200;

        if ($validator->fails()) {
            $response = implode(' ', $validator->messages()->all());
            $code = 422;
        } else {
            $trademark = Trademark::create($request->all());
        }

        return response()->json($response, $code);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Trademark  $trademark
     * @return \Illuminate\Http\Response
     */
    public function show(Trademark $trademark)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Trademark  $trademark
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trademark $trademark)
    {
        $validator = Validator::make($request->all(), [
            'title'         => 'bail | required | unique:trademarks,title,' . $trademark->id
        ], $this->messages);

        $response = ['success' => 'Actualización completada exitosamente.'];
        $code = 200;

        if ($validator->fails()) {
            $response = implode(' ', $validator->messages()->all());
            $code = 422;
        } else {
            $trademark->update($request->except('id'));
        }

        return response()->json($response, $code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Trademark  $trademark
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trademark $trademark)
    {
        $message = "Eliminación completada exitosamente.";
        $code = 200;

        if($trademark->cars()->count() > 0){
            $message = "No se puede eliminar la marca, ya que tiene carros asociados.";
            $code = 422;
        }else{
            $trademark->delete();
        }

        return response()->json(['message' => $message], $code);
    }
}
