<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Models\Car;
use App\Models\Trademark;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CarController extends Controller
{
    private $attributes = ['model', 'year'];

    private $rules = [
        'model' => 'required',
        'year' => 'required',
    ];

    private $messages = [
        'model.required' => 'El modelo es requerido.',
        'year.required' => 'El año es requerido.'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //make validation
        $validator = Validator::make($request->all(), $this->rules, $this->messages);

        if($validator->fails()){
            $errorString = implode(",",$validator->messages()->all());
            return Response()->json($errorString, 422);
        }else{
            $trademark = Trademark::find($request->trademark_id);

            $data['model'] = $request->model;
            $data['year'] = $request->year;

            $car = new Car($data);
            $car->trademark()->associate($trademark);
            $car->save();

            return Response()->json(['message'   =>  'Registro con éxito'], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Car $car)
    {
        //make validation
        $validator = Validator::make($request->all(), $this->rules, $this->messages);

        if($validator->fails()){
            $errorString = implode(",",$validator->errors()->all());
            return Response()->json($errorString, 422);
        }else{            
            $trademark = Trademark::find($request->trademark_id);

            $car->model = $request['model'];
            $car->year = $request['year'];

            $car->trademark()->associate($trademark);
            $car->save();
            return Response()->json(['message'   =>  'Registro con éxito'], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        $car->delete();

        return Response()->json(['message' => 'Registro eliminado exitosamente'], 200);
    }
}
