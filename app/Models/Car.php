<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;

    protected $fillable = [
        'model', 'year'
    ];

    public function trademark()
	{
	    return $this->belongsTo('App\Models\Trademark');
	}
}
