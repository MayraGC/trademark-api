<?php

namespace Database\Seeders;

use App\Models\Trademark;
use Illuminate\Database\Seeder;

class TrademarkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trademark = new Trademark();
        $trademark->title = 'BMW';
        $trademark->save();

        $trademark = new Trademark();
        $trademark->title = 'Mercedes-Benz.';
        $trademark->save();

        $trademark = new Trademark();
        $trademark->title = 'Audi';
        $trademark->save();

        $trademark = new Trademark();
        $trademark->title = 'Renault';
        $trademark->save();

        $trademark = new Trademark();
        $trademark->title = 'Ford';
        $trademark->save();

        $trademark = new Trademark();
        $trademark->title = 'Chevrolet';
        $trademark->save();

        $trademark = new Trademark();
        $trademark->title = 'Fiat';
        $trademark->save();

        $trademark = new Trademark();
        $trademark->title = 'Honda';
        $trademark->save();

        $trademark = new Trademark();
        $trademark->title = 'Hyundai';
        $trademark->save();

        $trademark = new Trademark();
        $trademark->title = 'Kia';
        $trademark->save();

        $trademark = new Trademark();
        $trademark->title = 'Mazda';
        $trademark->save();

        $trademark = new Trademark();
        $trademark->title = 'Nissan';
        $trademark->save();

        $trademark = new Trademark();
        $trademark->title = 'Toyota';
        $trademark->save();

        $trademark = new Trademark();
        $trademark->title = 'Volkswagen';
        $trademark->save();
    }
}
